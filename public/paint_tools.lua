mfl = math.floor

function distributeWork(w, h, artist)
    local maxv = w*h+w-1

    local ivl = maxv // #artist

    local idx, _next = 1, 0

    local index = {}

    for i=0,maxv,ivl do
        local idx = i
        local sidx = i
        
        for j=i,math.min(maxv, i+ivl) do
            idx = j
        end
        
        if maxv - idx <= ivl then
            table.insert(index, {sidx+_next, maxv})
            break
        end
        
        table.insert(index, {sidx+_next, idx})
        
        if _next == 0 then
            _next = 1
        end
        
    end

    for i=1,#artist do
        local mx, mv = table.unpack(index[i])
        for i=mx,mv do
            column = i % w
            row    = mfl(i / w)
        end
        modem.send(artist[i], ARTIST_PORT, "strip", mx, mv, w, h)
    end
end

function getArtists(modem, threshold)
    modem.broadcast(ARTIST_PORT, "answer", 1337)
    local artist = {}
    local current = os.time()
    while true do 
        local msg = table.pack(event.pull(0, "modem_message"))
        local tm = os.time()
        
        if #msg > 0 then
            table.insert(artist, msg[3])
            current = tm
        else 
            
            -- print(#artist, tm, current, table.unpack(msg))
            if tm - current > threshold then
                break
            end
            
        end
    end
    return artist
end